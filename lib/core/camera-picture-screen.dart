import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:flutter/services.dart';
// A screen that allows users to take a picture using a given camera.
class CameraPictureScreen extends StatefulWidget {
  CameraPictureScreen({
    super.key,
    required this.camera,
  }) ;

  final CameraDescription camera;

  @override
  CameraPictureScreenState createState() => CameraPictureScreenState();
   
} 

class CameraPictureScreenState extends State<CameraPictureScreen> {
 
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );

    _initializeControllerFuture = _controller.initialize();
    _controller.setFlashMode(FlashMode.off);

  }
  void on_take_photo() async {
    HapticFeedback.heavyImpact();
    await _initializeControllerFuture;
    final image = await _controller.takePicture();
    if (!mounted) return;
    Directory appDocDir = await getApplicationSupportDirectory();
    String appDocPath = appDocDir.path;
    File imageFile = File(image.path);
    imageFile.copy( appDocPath + "/image.jpg");
    print("taken photo ${image.path}");
    print("appDocPath ${appDocPath}");
    await Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(
                    // Pass the automatically generated path to
                    // the DisplayPictureScreen widget.
                    imagePath: image.path,
                    ),
                ),
            );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return Container(
                child : CameraPreview(_controller),
                decoration: BoxDecoration(
                    borderRadius : BorderRadius.circular(20),
                ),
              );
            }
            else {
              return const Center(child: CircularProgressIndicator());
            }
          }
        ),
    );
  }
}




class DisplayPictureScreen extends StatelessWidget {
  const DisplayPictureScreen({super.key, required this.imagePath});

  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Display the Picture')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(imagePath)),
    );
  }
}
