import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:hackaton/ui/pages/home-page.dart';
import 'package:camera/camera.dart';
class App extends StatelessWidget {
    App({super.key, required this.camera});
    final CameraDescription camera;

    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider(
            create: (context) => AppState(),
            child: MaterialApp(
                title: 'Flutter Demo dz',
                theme: ThemeData(
                    useMaterial3: true,
                    colorScheme: ColorScheme.fromSeed(seedColor: Colors.red.shade800),
                    scaffoldBackgroundColor: Colors.black54,
                ),
                debugShowCheckedModeBanner: false,
                home: HomePage(camera: camera),
            ),
        );
    }
}

class AppState extends ChangeNotifier {
    int counter = 0;
}
