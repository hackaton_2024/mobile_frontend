import 'package:flutter/material.dart';
import 'package:hackaton/app.dart';
import 'package:camera/camera.dart';
void main() async {
    final camera = await load_cameras();
    runApp(App(camera: camera));
}

Future<CameraDescription> load_cameras() async {
    WidgetsFlutterBinding.ensureInitialized();
    final cameras = await availableCameras();
    for (var camera in cameras) {
        
    }
    final firstCamera = cameras.first;
    return firstCamera;
}

