import 'package:flutter_tts/flutter_tts.dart';
class TTSService {
    TTSService();
    FlutterTts flutterTts = FlutterTts();
    void speak(String text) {
        flutterTts.speak(text);
    }
}
