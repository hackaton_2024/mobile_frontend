import 'package:flutter/material.dart';
import 'package:hackaton/ui/components/top-bar.dart';
import 'package:hackaton/core/camera-picture-screen.dart';
import 'package:camera/camera.dart';
import 'package:hackaton/services/tts-service.dart';

class HomePage extends StatelessWidget {

HomePage({super.key, required this.camera});
final CameraDescription camera;
final TTSService ttsService = TTSService();

@override
Widget build(BuildContext context) {
    var camScreen_key = GlobalKey<CameraPictureScreenState>();

    var camScreen = CameraPictureScreen(
        key: camScreen_key,
        camera: camera,
    );

    return Scaffold(
        appBar: TopBar(),        
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
                camScreen,
                Expanded(
                    child:Center(
                        child: FilledButton(onPressed: () async {

                            camScreen_key.currentState?.on_take_photo();
                            ttsService.speak("Photo taken");


                        }, child: Icon(Icons.camera_alt, size: 30),
                            style: FilledButton.styleFrom(
                                shape: CircleBorder(),
                                padding: EdgeInsets.all(20),
                            )),
                    ),
                ),
            ]
        ),
    );
}

}

